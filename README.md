
# Skywars
<br/>

**THE MOD IS IN MAINTENANCE MODE, I won't add any new feature.**

<br/>

### How do I play? <br/><hr/>

Skywars is a PvP minigame where players battle each other on floating islands until there is only one survivor remaining.
Loot from chests, collect weapons and resources and defeat them all!


<br/><br/>



### Translations <br/><hr/>

Check out [Weblate](https://translate.codeberg.org/projects/zughy-friends-minetest/skywars/) for translations


<br/><br/>


  
### Dependencies <hr/>

* default

* [arena_lib](https://gitlab.com/zughy-friends-minetest/arena_lib/).


Optional:
* [3d_armor](https://github.com/minetest-mods/3d_armor/commit/7f63df230c7c63d6ae898a96a99655f10d290b13) the supported versions are from this commit on;

* [ender_pearl](https://content.minetest.net/packages/giov4/enderpearl/).
