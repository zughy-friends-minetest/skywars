minetest.register_on_punchplayer(function(player, hitter, time_from_last_punch, tool_capabilities, dir, damage)
	local pl_name = player:get_player_name()
	local arena = arena_lib.get_arena_by_player(pl_name)

	if
		arena_lib.is_player_in_arena(pl_name, "skywars")
		and arena.players[pl_name]
		and arena.players_original_amount > 0 -- when the match actually starts
		and minetest.is_player(hitter)
	then
		local hitter_name = hitter:get_player_name()

		arena.players[pl_name].last_hit_by = hitter_name

		minetest.after(5, function ()
			if arena.players and arena.players[pl_name] and arena.players[pl_name].last_hit_by == hitter_name then
				arena.players[pl_name].last_hit_by = ""
			end
		end)
	end
end)