local get_player_by_name = minetest.get_player_by_name


function skywars.kill_players_out_map(arena)
    for pl_name in pairs(arena.players) do
        local player = get_player_by_name(pl_name)
        local pl_pos = player:get_pos()

        if pl_pos.y < arena.min_y then
            if arena.players then
                local last_hitter = arena.players[pl_name].last_hit_by
                if last_hitter ~= "" and arena.players[last_hitter] then
                    skywars.award_achievement(last_hitter, "skywars:push_em_all")
                end
            end
            player:set_hp(0)
        end
    end
end