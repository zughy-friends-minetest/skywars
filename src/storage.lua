local storage = minetest.get_mod_storage()
skywars.st = storage



function skywars.load_table(metadata_name)
    return minetest.deserialize(storage:get_string(metadata_name)) or {}
end



function skywars.store_table(metadata_name, table)
    storage:set_string(metadata_name, minetest.serialize(table))
end