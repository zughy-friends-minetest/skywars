minetest.register_node("skywars:test_node", {
    description = "Skywars test node, don't use it!",
    groups = {crumbly=1, soil=1, dig_immediate=3},
    tiles = {"sw_node_test.png"},
})