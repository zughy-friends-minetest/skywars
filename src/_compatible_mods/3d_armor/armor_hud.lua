local saved_huds = {}  -- id = hud



function skywars.HUD_armor_create(player)
    local pl_name = player:get_player_name()

    if saved_huds[pl_name] then return end

    local x_pos = 0.5
    local y_pos = 1
    local scale = 1.6
    local x_off = -32 * player:hud_get_hotbar_itemcount()
    local y_off = -20

    local head = player:hud_add({
        hud_elem_type = "image",
        position  = {x = x_pos, y = y_pos},
        offset = {x = x_off -25, y = y_off - 25},
        text      = "3d_armor_inv_helmet_bronze.png^[multiply:#000000",
        alignment = {x = 1},
        scale     = {x = scale, y = scale},
    })

    local torso = player:hud_add({
        hud_elem_type = "image",
        position  = {x = x_pos, y = y_pos},
        offset = {x = x_off, y = y_off - 25},
        text      = "3d_armor_inv_chestplate_bronze.png^[multiply:#000000",
        alignment = {x = 1},
        scale     = {x = scale, y = scale},
    })

    local legs = player:hud_add({
        hud_elem_type = "image",
        position  = {x = x_pos, y = y_pos},
        offset = {x = x_off -25, y = y_off},
        text      = "3d_armor_inv_leggings_bronze.png^[multiply:#000000",
        alignment = {x = 1},
        scale     = {x = scale, y = scale},
    })

    local feet = player:hud_add({
        hud_elem_type = "image",
        position  = {x = x_pos, y = y_pos},
        offset = {x = x_off, y = y_off},
        text      = "3d_armor_inv_boots_bronze.png^[multiply:#000000",
        alignment = {x = 1},
        scale     = {x = scale, y = scale},
    })

    saved_huds[pl_name] = { head = head, torso = torso, legs = legs, feet = feet}
end



function skywars.HUD_armor_remove(player)
    local pl_name = player:get_player_name()

    if not saved_huds[pl_name] then return end

    for _, id in pairs(saved_huds[pl_name]) do
        player:hud_remove(id)
    end

    saved_huds[pl_name] = nil
end



function skywars.HUD_armor_update(player, stack)
    local type = armor:get_element(stack:get_name())
    local txtr = stack:get_definition().inventory_image

    -- there's no way they can simply unequip a piece at the moment, so I don't run further checks
    player:hud_change(saved_huds[player:get_player_name()][type], "text", txtr)
end