minetest.register_on_joinplayer(function(player)
    if skywars_settings.remove_armors_on_join and minetest.get_modpath("3d_armor") then
      minetest.after(5, function() armor:remove_all(player) end)
    end
end)



function skywars.remove_armor(player)
    if minetest.get_modpath("3d_armor") then
        armor:remove_all(player)
        skywars.HUD_armor_remove(player)
    end
end



armor:register_on_equip(function(player, index, stack)
    if arena_lib.is_player_in_arena(player:get_player_name(), "skywars") then
        skywars.HUD_armor_update(player, stack)
    end
end)



dofile(minetest.get_modpath("skywars") .. "/src/_compatible_mods/3d_armor/auto_equip_armors.lua")
dofile(minetest.get_modpath("skywars") .. "/src/_compatible_mods/3d_armor/armor_hud.lua")