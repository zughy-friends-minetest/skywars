local S = minetest.get_translator("skywars")



if minetest.get_modpath("achievements_lib") then
	achvmt_lib.register_mod("skywars", {
		name = "Skywars",
		icon = "skywars_icon.png"
	})

	achvmt_lib.register_achievement("skywars:lone_wolf", {
		title = S("Lone Wolf"),
		description = S("Kill everyone"),
		additional_info = S("@1+ players required", 3),
		image = "sw_ach_lone_wolf.png",
		tier = "Silver"
	})

	achvmt_lib.register_achievement("skywars:fast_and_furious", {
		title = S("Fast and Furious"),
		description = S("Get a kill in the first 10 seconds"),
		image = "sw_ach_fast_and_furious.png",
		tier = "Gold"
	})

	
	achvmt_lib.register_achievement("skywars:push_em_all", {
		title = S("Push 'em All"),
		description = S("Push someone into the void"),
		image = "sw_ach_push_em_all.png",
		tier = "Bronze"
	})

	achvmt_lib.register_achievement("skywars:fearless", {
		title = S("Fearless"),
		description = S("Win without healing"),
		additional_info = S("@1+ players required", 3),
		image = "sw_ach_fearless.png",
		tier = "Silver"
	})
	dofile(minetest.get_modpath("skywars") .. "/src/_compatible_mods/achievements_lib/fearless.lua")
end



function skywars.award_achievement(pl_name, ach_name)
	if minetest.get_modpath("achievements_lib") then
		achvmt_lib.award(pl_name, ach_name)
	end
end