minetest.register_on_player_hpchange(function (player, hp_change, reason)
	local pl_name = player:get_player_name()
	local arena = arena_lib.get_arena_by_player(pl_name)

	if
		arena_lib.is_player_in_arena(pl_name, "skywars")
		and arena.players[pl_name]
		and arena.players_original_amount > 0 -- when the match actually starts
	then
		if hp_change > 0 then
			arena.players[pl_name].fearless = false
		end
	end
end)